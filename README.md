# SFDX Bitbucket Pipelines Example

This project is meant to demonstrate how bitbucket pipelines can be used in conjuction with Saleforce DX to provide a continuous integration solution.  This project requires that you have access to the Salesforce DX pilot and NPM installed.  NPM is not techinically a requirement to get this to work but a major convience.

## Initial Setup

1. Follow "Getting Started Guide" for SFDX

1. `git clone ssh://git@bitbucket.org/charlie_jonas/sfdx-pipelines-example.git`

1. `cd sfdx-pipelines`

1. `npm install`

1. Authorize the Hub Org: `sfdx force:auth:web:login -d --setdefaultdevhubusername` (w/ user created in step 1)

1. Authorize "target" org: `sfdx force:auth:web:login -a "target"`.  Login using creditials for a production, sandbox or developer org

## Development/Scratch Org Creation

simply run `npm run createenv`

This performs the following actions:

1. Creates a new scratch org

1. Pushes the source

1. runs `dataload` scripts

You can then open the org by runnning `sfdx force:org:open`

### Adding Appexchange packages

If you have any App Exchanges packages, simply add them to the end of the `precreateenv` script.  Example:
`sfdx force:org:create -s -f config/workspace-scratch-def.json -a 'defaultscratchorg' && sfdx force:packageversion:install --id 04t44000000VDdT`

## Development

I recommend using vscode for development.  The tooling for sfdx is currently pretty limited and the inline terminal makes it more bearable.

To automaticly detect file changes and push source, run:
`npm run push:watch`

## Running Tests

simply run `npm test`

## Continous Integration

This project uses bitbucket pipelines as a CI solution.

### Branch Strategy

Once Bitbucket Pipelines have been enabled, any commit to master will automaticly be deployed.  Because of this, commiting directly to master should be disabled and only be through pull requests.

Commits to non-master branches will trigger a validation build against a scratch org. We could also prevent merges into master unless the commit has a successful build.

### Setup

The first thing we need to do is setup JWT authinication on our HUB org.

1. Create a [self signed certificate](https://devcenter.heroku.com/articles/ssl-certificate-self)

1. Add encrypted signing key to build dir

    - Generate a AES key & IV. Store them some place safe. We will need them later (I used [this tool](https://asecuritysite.com/encryption/keygen)... Not sure how secure that really is)
    - `openssl enc -aes-256-cbc -in server.key -out build/server.key.enc -K [AES_KEY] -iv [IV_KEY]` (this should replace the current `server.key.enc`)

1. Create a connected app on the HUB org

    - Enable Digital Signitures and uploaded the `.crt` created in the step 1
    - Set callback URL `http://localhost:1717/OauthRedirect`
    - Grant `Access your basic information`, `Perform requests on your behalf at any time` & `full access` scope (not sure if full access is really needed)

1. Authorize the connected for you user by going to: `https://login.salesforce.com/services/oauth2/authorize?client_id=[clientId]&redirect_uri=[redirectUri]&response_type=code`

1. Login to your target org.  Repeat steps 3 & 4 to create a connected app that can be used for deployment pipeline.

1. In bitbucket, go to `Settings->Pipelines->Enviorment` and add the following values:

    - `AESKEY` (secured): AES key from encryption step
    - `IVKEY` (secured): IV key from encryption
    - `CONSUMERKEY_HUB`: the Consumer Key from the hub org connected app
    - `USERNAME_HUB`: the Username that will connect to the Hub org
    - `CONSUMERKEY_TARGET`: the Consumer Key from the target org connected app
    - `USERNAME_TARGET`: the Username that will connect to the target org

### CI Configuration

The CI build process is defined in bitbucket.pipelines.yml.  More information about pipelines configuration can be found [here](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html).

I created an [docker image](https://hub.docker.com/r/illbilly/sfdx/) for SFDX so that we don't have to wait while the SFDX dependencies install each run.

Note:
In order to easily debug the pipelines.yml, you can fire up a docker container with this command: `docker run -it --volume=/Users/you/code/repo-dir:/repo-dir --workdir="/repo-dir" --memory=4g --entrypoint=/bin/bash illbilly/sfdx`.  Once started you can run the individual commands from the yaml.  Be careful as any changes made while in the container will update the repository.


//test1 checkin